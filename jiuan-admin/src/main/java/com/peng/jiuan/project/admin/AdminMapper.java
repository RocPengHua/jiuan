package com.peng.jiuan.project.admin;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.peng.jiuan.result.ResultMap;
import org.springframework.stereotype.Repository;

/**
 * Mapper: sys_admin -- 系统管理员表
 * @author peng 
 */

@Mapper
@Repository
public interface AdminMapper {

	/**
	 * 增  
	 * @param a 实体对象 
	 * @return 受影响行数 
	 */
	int add(Admin a);

	/**
	 * 删  
	 * @param id 要删除的数据id  
	 * @return 受影响行数 
	 */
	int delete(Long id);	 

	/** 
	 * 改  
	 * @param a 实体对象 
	 * @return 受影响行数 
	 */
	int update(Admin a);

	/** 
	 * 查 - 根据id  
	 * @param id 要查询的数据id 
	 * @return 实体对象 
	 */
	Admin getById(Long id);	 

	/**
	 * 查集合 - 根据条件（参数为空时代表忽略指定条件）
	 * @param so 参数集合 
	 * @return 数据列表 
	 */
	List<Admin> getList(ResultMap so);

	/**
	 * 查询，根据name
	 * @param name
	 * @return
	 */
	Admin getByName(String name);

	/**
	 * 查询，根据 phone
	 * @param phone
	 * @return
	 */
	Admin getByPhone(String phone);
}
