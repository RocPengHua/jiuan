package com.peng.jiuan.project.login;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 账号相关 
 *
 */
@Mapper
public interface AccAdminMapper {

	/**
	 * 指定id的账号成功登录一次 
	 * @param id
	 * @param loginIp
	 * @return
	 */
	public int successLogin(@Param("id")long id, @Param("loginIp")String loginIp);
	
}
