package com.peng.jiuan.project.role;

import java.util.List;

import cn.dev33.satoken.annotation.SaMode;
import com.peng.jiuan.satoken.AuthConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import com.peng.jiuan.project.public4mapper.Jiuan;
import com.peng.jiuan.result.AjaxException;
import com.peng.jiuan.result.AjaxJson;
import com.peng.jiuan.result.ResultMap;
import com.peng.jiuan.satoken.StpUserUtil;

import cn.dev33.satoken.annotation.SaCheckPermission;


/**
 * Controller: sys_role -- 系统角色表
 * @author peng
 */
@RestController
@RequestMapping("/Role/")
public class RoleController {

	/** 底层 Mapper 对象 */
	@Autowired
	RoleMapper roleMapper;

	/** 增 */
	@PostMapping("add")
	@SaCheckPermission(value = {AuthConstant.R1,AuthConstant.ROLE_LIST},mode = SaMode.AND)
	@Transactional(rollbackFor = Exception.class)
	public AjaxJson add(Role r){
		if(roleMapper.getById(r.getId()) != null) {
			return AjaxJson.getError("此id已存在，请更换");
		}
		RoleUtil.checkRoleThrow(r);
		int line = roleMapper.add(r);
		AjaxException.throwByLine(line, "添加失败");
		// 返回这个对象
		long id = r.getId();
		if(id == 0) {
			id = Jiuan.publicMapper.getPrimarykey();
		}
		return AjaxJson.getSuccessData(roleMapper.getById(id));
	}

	/** 删 */
	@PostMapping("delete")
	@SaCheckPermission(value = {AuthConstant.R1,AuthConstant.ROLE_LIST},mode = SaMode.AND)
	public AjaxJson delete(Long id){
		int line = roleMapper.delete(id);
		return AjaxJson.getByLine(line);
	}

	/** 删 - 根据id列表 */
	@PostMapping("deleteByIds")
	@SaCheckPermission(value = {AuthConstant.R1,AuthConstant.ROLE_LIST},mode = SaMode.AND)
	public AjaxJson deleteByIds(){
		List<Long> ids = ResultMap.getRequestResultMap().getListByComma("ids", long.class);
		int line = Jiuan.publicMapper.deleteByIds(Role.TABLE_NAME, ids);
		return AjaxJson.getByLine(line);
	}

	/** 改 */
	@PostMapping("update")
	@SaCheckPermission(value = {AuthConstant.R1,AuthConstant.ROLE_LIST},mode = SaMode.AND)
	public AjaxJson update(Role r){
		int line = roleMapper.update(r);
		return AjaxJson.getByLine(line);
	}

	/** 查 - 根据id */
	@PostMapping("getById")
	@SaCheckPermission(AuthConstant.R99)
	public AjaxJson getById(Long id){
		Role r = roleMapper.getById(id);
		return AjaxJson.getSuccessData(r);
	}

	/** 查集合 - 根据条件（参数为空时代表忽略指定条件） */
	@PostMapping("getList")
	@SaCheckPermission(AuthConstant.R99)
	public AjaxJson getList() {
		ResultMap so = ResultMap.getRequestResultMap();
		List<Role> list = roleMapper.getList(so.startPage());
		return AjaxJson.getPageData(so.getDataCount(), list);
	}



	// ------------------------- 前端接口 -------------------------


	/** 改 - 不传不改 [G] */
	@PostMapping("updateByNotNull")
	@SaCheckPermission(AuthConstant.R99)
	public AjaxJson updateByNotNull(Long id){

		AjaxException.throwBy(true, "如需正常调用此接口，请删除此行代码");
		// 鉴别身份，是否为数据创建者
		long userId = Jiuan.publicMapper.getColumnByIdToLong(Role.TABLE_NAME, "user_id", id);
		AjaxException.throwBy(userId != StpUserUtil.getLoginIdAsLong(), "此数据您无权限修改");
		// 开始修改 (请只保留需要修改的字段)
		ResultMap so = ResultMap.getRequestResultMap();
		so.clearNotIn("id", "name", "info", "isLock", "createTime").clearNull().humpToLineCase();
		int line = Jiuan.publicMapper.updateByResultMapById(Role.TABLE_NAME, so, id);
		return AjaxJson.getByLine(line);
	}







}