package com.peng.jiuan.project.admin;

import java.util.List;

import cn.dev33.satoken.stp.StpUtil;
import com.peng.jiuan.project.admin4password.AdminPasswordService;
import com.peng.jiuan.satoken.AuthConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.peng.jiuan.project.public4mapper.Jiuan;
import com.peng.jiuan.result.AjaxJson;
import com.peng.jiuan.result.ResultMap;

import cn.dev33.satoken.annotation.SaCheckPermission;


/**
 * Controller: sys_admin -- 系统管理员表
 * @author peng
 */
@RestController
@RequestMapping("/Admin/")
public class AdminController {

	@Autowired
	AdminMapper adminMapper;

	@Autowired
	AdminService adminService;

	@Autowired
	AdminPasswordService adminPasswordService;

	/** 增  */
	@PostMapping("add")
	@SaCheckPermission(AuthConstant.ADMIN_LIST)
	AjaxJson add(Admin admin){
		long id = adminService.add(admin);
		return AjaxJson.getSuccessData(id);
	}

	/** 删 */
	@PostMapping("delete")
	@SaCheckPermission(AuthConstant.ADMIN_LIST)
	AjaxJson delete(long id){
		// 不能自己删除自己
		if(StpUtil.getLoginIdAsLong() == id) {
			return AjaxJson.getError("不能自己删除自己");
		}
		int line = adminMapper.delete(id);
		return AjaxJson.getByLine(line);
	}

	/** 删 - 根据id列表 */
	@PostMapping("deleteByIds")
	@SaCheckPermission(AuthConstant.ADMIN_LIST)
	AjaxJson deleteByIds(){
		// 不能自己删除自己
		List<Long> ids = ResultMap.getRequestResultMap().getListByComma("ids", long.class);
		if(ids.contains(StpUtil.getLoginIdAsLong())) {
			return AjaxJson.getError("不能自己删除自己");
		}
		// 开始删除
		int line = Jiuan.publicMapper.deleteByIds("sys_admin", ids);
		return AjaxJson.getByLine(line);
	}

	/** 改  -  name */
	@PostMapping("update")
	@SaCheckPermission(AuthConstant.ADMIN_LIST)
	AjaxJson update(Admin obj){
		AdminUtil.checkName(obj.getId(), obj.getName());
		int line = adminMapper.update(obj);
		return AjaxJson.getByLine(line);
	}

	/** 查  */
	@PostMapping("getById")
	@SaCheckPermission(AuthConstant.ADMIN_LIST)
	AjaxJson getById(long id){
		Object data = adminMapper.getById(id);
		return AjaxJson.getSuccessData(data);
	}

	/** 查 - 集合 */
	@PostMapping("getList")
	@SaCheckPermission(AuthConstant.ADMIN_LIST)
	AjaxJson getList(){
		ResultMap so = ResultMap.getRequestResultMap();
		List<Admin> list = adminMapper.getList(so.startPage());
		return AjaxJson.getPageData(so.getDataCount(), list);
	}

	/** 改密码 */
	@PostMapping("updatePassword")
	@SaCheckPermission(AuthConstant.ADMIN_LIST)
	AjaxJson updatePassword(long id, String password){
		int line = adminPasswordService.updatePassword(id, password);
		return AjaxJson.getByLine(line);
	}

	/** 改头像  */
	@PostMapping("updateAvatar")
	@SaCheckPermission(AuthConstant.ADMIN_LIST)
	AjaxJson updateAvatar(long id, String avatar){
		int line = Jiuan.publicMapper.updateColumnById("sys_admin", "avatar", avatar, id);
		return AjaxJson.getByLine(line);
	}

	/** 改状态  */
	@PostMapping("updateStatus")
	@SaCheckPermission(AuthConstant.R1)
	public AjaxJson updateStatus(long adminId, int status) {

		// 验证对方是否为超管(保护超管不受摧残)
		if(StpUtil.hasPermission(adminId, AuthConstant.R1)){
			return AjaxJson.getError("抱歉，对方角色为系统超级管理员，您暂无权限操作");
		}

		// 修改状态
		Jiuan.publicMapper.updateColumnById("sys_admin", "status", status, adminId);
		// 如果是禁用，就停掉其秘钥有效果性，使其账号的登录状态立即无效
		if(status == 2) {
			StpUtil.logout(adminId);
		}
		return AjaxJson.getSuccess();
	}

	/** 改角色  */
	@PostMapping("updateRole")
	@SaCheckPermission(AuthConstant.ADMIN_LIST)
	AjaxJson updateRole(long id, String roleId){
		int line = Jiuan.publicMapper.updateColumnById("sys_admin", "role_id", roleId, id);
		return AjaxJson.getByLine(line);
	}

	/** 返回当前admin信息  */
	@PostMapping("getByCurr")
	AjaxJson getByCurr() {
		Admin admin = AdminUtil.getCurrAdmin();
		return AjaxJson.getSuccessData(admin);
	}

	/** 当前admin修改信息 */
	@PostMapping("updateInfo")
	@SaCheckPermission(AuthConstant.ADMIN_LIST)
	AjaxJson updateInfo(Admin obj){
		obj.setId(StpUtil.getLoginIdAsLong());
		AdminUtil.checkName(obj.getId(), obj.getName());
		int line = adminMapper.update(obj);
		return AjaxJson.getByLine(line);
	}
}