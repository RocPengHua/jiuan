package com.peng.jiuan.project.dept;

import java.io.Serializable;
import java.util.*;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * Model: sys_dept -- 部门表
 * @author peng 
 */
@Data
@Accessors(chain = true)
public class Dept implements Serializable {

	// ---------- 模块常量 ----------
	/**
	 * 序列化版本id 
	 */
	private static final long serialVersionUID = 1L;	
	/**
	 * 此模块对应的表名 
	 */
	public static final String TABLE_NAME = "sys_dept";	
	/**
	 * 此模块对应的权限码 
	 */
	public static final String PERMISSION_CODE = "dept";	


	// ---------- 表中字段 ----------
	/**
	 * 记录id 
	 */
	public Long id;	

	/**
	 * 部门名称 
	 */
	public String name;	

	/**
	 * 部门介绍 
	 */
	public String intro;	

	/**
	 * 父部门id 
	 */
	public Long parentId;	

	/**
	 * 创建日期 
	 */
	public Date createTime;	





	


}
