package com.peng.jiuan.project.dept;

import java.util.List;

import com.peng.jiuan.result.ResultMap;
import org.apache.ibatis.annotations.Mapper;

import org.springframework.stereotype.Repository;

/**
 * Mapper: sys_dept -- 部门表
 * @author peng 
 */

@Mapper
@Repository
public interface DeptMapper {

	/**
	 * 增  
	 * @param d 实体对象 
	 * @return 受影响行数 
	 */
	int add(Dept d);

	/**
	 * 删  
	 * @param id 要删除的数据id  
	 * @return 受影响行数 
	 */
	int delete(Long id);	 

	/** 
	 * 改  
	 * @param d 实体对象 
	 * @return 受影响行数 
	 */
	int update(Dept d);

	/** 
	 * 查 - 根据id  
	 * @param id 要查询的数据id 
	 * @return 实体对象 
	 */
	Dept getById(Long id);	 

	/**
	 * 查集合 - 根据条件（参数为空时代表忽略指定条件）
	 * @param so 参数集合 
	 * @return 数据列表 
	 */
	List<Dept> getList(ResultMap so);


}
