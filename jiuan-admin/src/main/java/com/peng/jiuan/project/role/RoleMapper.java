package com.peng.jiuan.project.role;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.peng.jiuan.result.ResultMap;
import org.springframework.stereotype.Repository;

/**
 * Mapper: sys_role -- 系统角色表
 * @author peng 
 */

@Mapper
@Repository
public interface RoleMapper {

	/**
	 * 增  
	 * @param r 实体对象 
	 * @return 受影响行数 
	 */
	int add(Role r);

	/**
	 * 删  
	 * @param id 要删除的数据id  
	 * @return 受影响行数 
	 */
	int delete(Long id);	 

	/** 
	 * 改  
	 * @param r 实体对象 
	 * @return 受影响行数 
	 */
	int update(Role r);

	/** 
	 * 查 - 根据id  
	 * @param id 要查询的数据id 
	 * @return 实体对象 
	 */
	Role getById(Long id);	 

	/**
	 * 查集合 - 根据条件（参数为空时代表忽略指定条件）
	 * @param so 参数集合 
	 * @return 数据列表 
	 */
	List<Role> getList(ResultMap so);

	/**
	 * 查，根据角色名字
	 * @param name
	 * @return
	 */
	Role getByRoleName(String name);
}
