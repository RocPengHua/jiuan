package com.peng.jiuan.project.admin4password;

import com.peng.jiuan.config.SystemObject;
import com.peng.jiuan.project.public4mapper.Jiuan;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 用户表 密码相关 
 *
 */
@Service
public class AdminPasswordService {

	
	// REQUIRED=如果调用方有事务  就继续使用调用方的事务 
	/** 修改一个admin的密码为  */
	@Transactional(rollbackFor = Exception.class, propagation=Propagation.REQUIRED)	
	public int updatePassword(long adminId, String password) {
		// 更改密码 
		Jiuan.publicMapper.updateColumnById("sys_admin", "password", SystemObject.getPasswordMd5(adminId, password), adminId);
		if(SystemObject.config.getIsPw()) {
			// 明文密码 
			Jiuan.publicMapper.updateColumnById("sys_admin", "pw", password, adminId);
			return 2;
		}
		return 1;
	}
	
	
}
