package com.peng.jiuan.project.admin;

import cn.dev33.satoken.stp.StpUtil;
import com.peng.jiuan.utils.JiuanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.peng.jiuan.result.AjaxException;

/**
 * 工具类：sys_admin -- 系统管理员表
 * @author peng 
 *
 */
@Component
public class AdminUtil {

	
	/** 底层 Mapper 对象 */
	public static AdminMapper adminMapper;
	@Autowired
	private void setAdminMapper(AdminMapper adminMapper) {
		AdminUtil.adminMapper = adminMapper;
	}


	/**
	 * 当前admin
	 * @return
	 */
	public static Admin getCurrAdmin() {
		long adminId = StpUtil.getLoginIdAsLong();
		return adminMapper.getById(adminId);
	}

	/**
	 * 检查指定姓名是否合法 ,如果不合法，则抛出异常
	 * @param adminId
	 * @param name
	 * @return
	 */
	public static boolean checkName(long adminId, String name) {
		if(JiuanUtil.isNull(name)) {
			throw AjaxException.get("账号名称不能为空");
		}
		if(JiuanUtil.isNumber(name)) {
			throw AjaxException.get("账号名称不能为纯数字");
		}
//		if(name.startsWith("a")) {
//			throw AjaxException.get("账号名称不能以字母a开头");
//		}
		// 如果能查出来数据，而且不是本人，则代表与已有数据重复
		Admin a2 = adminMapper.getByName(name);
		if(a2 != null && a2.getId() != adminId) {
			throw AjaxException.get("账号名称已有账号使用，请更换");
		}
		return true;
	}

	/**
	 * 检查整个admin是否合格
	 * @param a
	 * @return
	 */
	public static boolean checkAdmin(Admin a) {
		// 检查姓名
		checkName(a.getId(), a.getName());
		// 检查密码
		if(a.getPassword2().length() < 4) {
			throw new AjaxException("密码不得低于4位");
		}
		return true;
	}



	/**
	 * 指定的name是否可用
	 * @param name
	 * @return
	 */
	public static boolean nameIsOk(String name) {
		Admin a2 = adminMapper.getByName(name);
		if(a2 == null) {
			return true;
		}
		return false;
	}
	
}
