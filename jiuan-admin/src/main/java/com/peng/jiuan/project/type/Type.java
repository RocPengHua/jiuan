package com.peng.jiuan.project.type;

import java.io.Serializable;
import java.util.*;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * Model: sys_type -- 分类表
 * @author peng 
 */
@Data
@Accessors(chain = true)
public class Type implements Serializable {

	// ---------- 模块常量 ----------
	/**
	 * 序列化版本id 
	 */
	private static final long serialVersionUID = 1L;	
	/**
	 * 此模块对应的表名 
	 */
	public static final String TABLE_NAME = "sys_type";	
	/**
	 * 此模块对应的权限码 
	 */
	public static final String PERMISSION_CODE = "type";	


	// ---------- 表中字段 ----------
	/**
	 * 记录id 
	 */
	public Long id;	

	/**
	 * 分类名字 
	 */
	public String name;	

	/**
	 * 分类图标 
	 */
	public String icon;	

	/**
	 * 排序索引 
	 */
	public Integer sort;	

	/**
	 * 创建日期 
	 */
	public Date createTime;	



	// ---------- 额外字段 ----------
	/**
	 * 商品数量 
	 */
	public Long serGoodsCount;	



	


}
