package com.peng.jiuan.project.login;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.stp.StpUtil;
import com.peng.jiuan.project.admin.Admin;
import com.peng.jiuan.project.admin.AdminUtil;
import com.peng.jiuan.project.jiuancfg.JiuanCfgUtil;
import com.peng.jiuan.project.role4permission.RolePermissionService;
import com.peng.jiuan.result.AjaxJson;
import com.peng.jiuan.result.ResultMap;
import com.peng.jiuan.utils.JiuanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * admin账号相关的接口 
 * @author kong
 *
 */
@RestController
@RequestMapping("/AccAdmin/")
public class AccAdminController {

	
	@Autowired
	AccAdminService accAdminService;
	
	@Autowired
	RolePermissionService rolePermissionService;
	
	
	/** 账号、密码登录  */
	@PostMapping("doLogin")
	AjaxJson doLogin(String key, String password) {
		// 1、验证参数 
		if(JiuanUtil.isOneNull(key, password)) {
			return AjaxJson.getError("请提供key与password参数");
		}
		return accAdminService.doLogin(key, password);
	}
	
	
	/** 退出登录  */
	@PostMapping("doExit")
	AjaxJson doExit() {
		StpUtil.logout();
		return AjaxJson.getSuccess();
	}
	

	/** 管理员登录后台时需要返回的信息 */
	@PostMapping("fristOpenAdmin")
	@SaCheckLogin
	AjaxJson fristOpenAdmin(HttpServletRequest request) {
		// 当前admin
		Admin admin = AdminUtil.getCurrAdmin();
		
		// 组织参数 (admin信息，权限信息，配置信息)
		ResultMap map = new ResultMap();
		map.set("admin", AdminUtil.getCurrAdmin());
		map.set("per_list", rolePermissionService.getPcodeByRid2(admin.getRoleId()));
		map.set("app_cfg", JiuanCfgUtil.getAppCfg());
		return AjaxJson.getSuccessData(map); 
	}
	
	
	
	
	
}
