package com.peng.jiuan.project.role;

import com.peng.jiuan.project.admin.AdminUtil;
import com.peng.jiuan.result.AjaxJson;
import com.peng.jiuan.utils.JiuanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.peng.jiuan.result.AjaxException;

/**
 * 工具类：sys_role -- 系统角色表
 * @author peng 
 *
 */
@Component
public class RoleUtil {

	
	/** 底层 Mapper 对象 */
	public static RoleMapper roleMapper;
	@Autowired
	private void setRoleMapper(RoleMapper roleMapper) {
		RoleUtil.roleMapper = roleMapper;
	}


	/**
	 * 获取当前会话的roleId
	 */
	public static long getCurrRoleId() {
		return AdminUtil.getCurrAdmin().getRoleId();
	}

	/**
	 * 验证一个SysRole对象 是否符合标准
	 * @param s
	 * @return
	 */
	static AjaxJson checkRole(Role s) {

		// 1、名称相关
		if(JiuanUtil.isNull(s.getName())) {
			return AjaxJson.getError("昵称不能为空");
		}
		// 2、如果该名称已存在，并且不是当前角色
		Role s2 = roleMapper.getByRoleName(s.getName());
		if(s2 != null && !s2.getId().equals(s.getId())) {
			return AjaxJson.getError("昵称与已有角色重复，请更换");
		}

		// 重重检验，最终合格
		return AjaxJson.getSuccess();
	}

	/**
	 * 验证一个Role是否符合标准, 不符合则抛出异常
	 * @param s
	 */
	static void checkRoleThrow(Role s) {
		AjaxJson aj = checkRole(s);
		if(aj.getCode() != AjaxJson.CODE_SUCCESS){
			throw AjaxException.get(aj.getMsg());
		}
	}
	
}
