package com.peng.jiuan.project.ser_goods;

import java.io.Serializable;
import java.util.*;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * Model: ser_goods -- 商品表
 * @author peng 
 */
@Data
@Accessors(chain = true)
public class SerGoods implements Serializable {

	// ---------- 模块常量 ----------
	/**
	 * 序列化版本id 
	 */
	private static final long serialVersionUID = 1L;	
	/**
	 * 此模块对应的表名 
	 */
	public static final String TABLE_NAME = "ser_goods";	
	/**
	 * 此模块对应的权限码 
	 */
	public static final String PERMISSION_CODE = "ser-goods";	


	// ---------- 表中字段 ----------
	/**
	 * 记录id 
	 */
	public Long id;	

	/**
	 * 商品名称 
	 */
	public String name;	

	/**
	 * 商品头像 
	 */
	public String avatar;	

	/**
	 * 轮播图片 
	 */
	public String imageList;	

	/**
	 * 图文介绍 
	 */
	public String content;	

	/**
	 * 商品价格 
	 */
	public Integer money;	

	/**
	 * 所属分类 
	 */
	public Long typeId;	

	/**
	 * 剩余库存 
	 */
	public Integer stockCount;	

	/**
	 * 商品状态 (1=上架,2=下架) 
	 */
	public Integer status;	

	/**
	 * 创建日期 
	 */
	public Date createTime;	

	/**
	 * 更新日期 
	 */
	public Date updateTime;	



	// ---------- 额外字段 ----------
	/**
	 * 所属分类 
	 */
	public String typeName;	



	


}
