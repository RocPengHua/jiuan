package com.peng.jiuan.project.ser_goods;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import com.peng.jiuan.project.public4mapper.Jiuan;
import com.peng.jiuan.result.AjaxException;
import com.peng.jiuan.result.AjaxJson;
import com.peng.jiuan.result.ResultMap;
import com.peng.jiuan.satoken.StpUserUtil;

import cn.dev33.satoken.annotation.SaCheckPermission;


/**
 * Controller: ser_goods -- 商品表
 * @author peng
 */
@RestController
@RequestMapping("/SerGoods/")
public class SerGoodsController {

	/** 底层 Mapper 对象 */
	@Autowired
	SerGoodsMapper serGoodsMapper;

	/** 增 */
	@PostMapping("add")
	@SaCheckPermission(SerGoods.PERMISSION_CODE)
	@Transactional(rollbackFor = Exception.class)
	public AjaxJson add(SerGoods s){
		serGoodsMapper.add(s);
		s = serGoodsMapper.getById(Jiuan.publicMapper.getPrimarykey());
		return AjaxJson.getSuccessData(s);
	}

	/** 删 */
	@PostMapping("delete")
	@SaCheckPermission(SerGoods.PERMISSION_CODE)
	public AjaxJson delete(Long id){
		int line = serGoodsMapper.delete(id);
		return AjaxJson.getByLine(line);
	}

	/** 删 - 根据id列表 */
	@PostMapping("deleteByIds")
	@SaCheckPermission(SerGoods.PERMISSION_CODE)
	public AjaxJson deleteByIds(){
		List<Long> ids = ResultMap.getRequestResultMap().getListByComma("ids", long.class);
		int line = Jiuan.publicMapper.deleteByIds(SerGoods.TABLE_NAME, ids);
		return AjaxJson.getByLine(line);
	}

	/** 改 */
	@PostMapping("update")
	@SaCheckPermission(SerGoods.PERMISSION_CODE)
	public AjaxJson update(SerGoods s){
		int line = serGoodsMapper.update(s);
		return AjaxJson.getByLine(line);
	}

	/** 查 - 根据id */
	@PostMapping("getById")
	@SaCheckPermission(SerGoods.PERMISSION_CODE)
	public AjaxJson getById(Long id){
		SerGoods s = serGoodsMapper.getById(id);
		return AjaxJson.getSuccessData(s);
	}

	/** 查集合 - 根据条件（参数为空时代表忽略指定条件） */
	@PostMapping("getList")
	@SaCheckPermission(SerGoods.PERMISSION_CODE)
	public AjaxJson getList() {
		ResultMap so = ResultMap.getRequestResultMap();
		List<SerGoods> list = serGoodsMapper.getList(so.startPage());
		return AjaxJson.getPageData(so.getDataCount(), list);
	}



	// ------------------------- 前端接口 -------------------------


	/** 改 - 不传不改 [G] */
	@PostMapping("updateByNotNull")
	@SaCheckPermission(SerGoods.PERMISSION_CODE)
	public AjaxJson updateByNotNull(Long id){

		AjaxException.throwBy(true, "如需正常调用此接口，请删除此行代码");
		// 鉴别身份，是否为数据创建者
		long userId = Jiuan.publicMapper.getColumnByIdToLong(SerGoods.TABLE_NAME, "user_id", id);
		AjaxException.throwBy(userId != StpUserUtil.getLoginIdAsLong(), "此数据您无权限修改");
		// 开始修改 (请只保留需要修改的字段)
		ResultMap so = ResultMap.getRequestResultMap();
		so.clearNotIn("id", "name", "avatar", "imageList", "content", "money", "typeId", "stockCount", "status", "createTime", "updateTime").clearNull().humpToLineCase();
		int line = Jiuan.publicMapper.updateByResultMapById(SerGoods.TABLE_NAME, so, id);
		return AjaxJson.getByLine(line);
	}







}