package com.peng.jiuan.project.dept;

import com.peng.jiuan.result.AjaxException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * 工具类：sys_dept -- 部门表
 * @author peng 
 *
 */
@Component
public class DeptUtil {

	
	/** 底层 Mapper 对象 */
	public static DeptMapper deptMapper;
	@Autowired
	private void setDeptMapper(DeptMapper deptMapper) {
		DeptUtil.deptMapper = deptMapper;
	}
	
	
	/** 
	 * 将一个 Dept 对象进行进行数据完整性校验 (方便add/update等接口数据校验) [G] 
	 */
	static void check(Dept d) {
		AjaxException.throwByIsNull(d.id, "[记录id] 不能为空");		// 验证: 记录id
		AjaxException.throwByIsNull(d.name, "[部门名称] 不能为空");		// 验证: 部门名称 
		AjaxException.throwByIsNull(d.intro, "[部门介绍] 不能为空");		// 验证: 部门介绍 
		AjaxException.throwByIsNull(d.parentId, "[父部门id] 不能为空");		// 验证: 父部门id 
		AjaxException.throwByIsNull(d.createTime, "[创建日期] 不能为空");		// 验证: 创建日期 
	}

	/** 
	 * 获取一个Dept (方便复制代码用) [G] 
	 */ 
	static Dept getDept() {
		Dept d = new Dept();	// 声明对象 
		d.id = 0L;		// 记录id 
		d.name = "";		// 部门名称 
		d.intro = "";		// 部门介绍 
		d.parentId = 0L;		// 父部门id 
		d.createTime = new Date();		// 创建日期 
		return d;
	}
	
	
	
	
	
}
