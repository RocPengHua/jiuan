package com.peng.jiuan.project.role;

import java.io.Serializable;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * Model: sys_role -- 系统角色表
 * @author peng 
 */
@Data
@Accessors(chain = true)
public class Role implements Serializable {

	// ---------- 模块常量 ----------
	/**
	 * 序列化版本id 
	 */
	private static final long serialVersionUID = 1L;	
	/**
	 * 此模块对应的表名 
	 */
	public static final String TABLE_NAME = "sys_role";	
	/**
	 * 此模块对应的权限码 
	 */
	public static final String PERMISSION_CODE = "role";	


	// ---------- 表中字段 ----------
	/**
	 * 角色id，--主键、自增 
	 */
	public Long id;	

	/**
	 * 角色名称, 唯一约束 
	 */
	public String name;	

	/**
	 * 角色详细描述 
	 */
	public String info;	

	/**
	 * 是否锁定(1=是,2=否), 锁定之后不可随意删除, 防止用户误操作 
	 */
	public Integer isLock;	

	/**
	 * 创建时间 
	 */
	public String createTime;	





	


}
