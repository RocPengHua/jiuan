package com.peng.jiuan.project.type;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.peng.jiuan.result.AjaxException;
import java.util.*;

/**
 * 工具类：sys_type -- 分类表
 * @author peng 
 *
 */
@Component
public class TypeUtil {

	
	/** 底层 Mapper 对象 */
	public static TypeMapper typeMapper;
	@Autowired
	private void setTypeMapper(TypeMapper typeMapper) {
		TypeUtil.typeMapper = typeMapper;
	}
	
	
	/** 
	 * 将一个 Type 对象进行进行数据完整性校验 (方便add/update等接口数据校验) [G] 
	 */
	static void check(Type t) {
		AjaxException.throwByIsNull(t.id, "[记录id] 不能为空");		// 验证: 记录id 
		AjaxException.throwByIsNull(t.name, "[分类名字] 不能为空");		// 验证: 分类名字 
		AjaxException.throwByIsNull(t.icon, "[分类图标] 不能为空");		// 验证: 分类图标 
		AjaxException.throwByIsNull(t.sort, "[排序索引] 不能为空");		// 验证: 排序索引 
		AjaxException.throwByIsNull(t.createTime, "[创建日期] 不能为空");		// 验证: 创建日期 
	}

	/** 
	 * 获取一个Type (方便复制代码用) [G] 
	 */ 
	static Type getType() {
		Type t = new Type();	// 声明对象 
		t.id = 0L;		// 记录id 
		t.name = "";		// 分类名字 
		t.icon = "";		// 分类图标 
		t.sort = 0;		// 排序索引 
		t.createTime = new Date();		// 创建日期 
		return t;
	}
	
	
	
	
	
}
