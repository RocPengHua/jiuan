package com.peng.jiuan.project.admin;

import com.peng.jiuan.project.admin4password.AdminPasswordService;
import com.peng.jiuan.project.public4mapper.Jiuan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.dev33.satoken.stp.StpUtil;

/**
 * Service: admin管理员
 * @author kong
 *
 */
@Service
public class AdminService {

	
	@Autowired
	AdminMapper adminMapper;
	
	@Autowired
	AdminPasswordService adminPasswordService;
	
	
	/**
	 * 管理员添加一个管理员 
	 * @param admin
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class, propagation=Propagation.REQUIRED)	
	public long add(Admin admin) {
		// 检查姓名是否合法
		AdminUtil.checkAdmin(admin);
		
		// 创建人，为当前账号  
		admin.setCreateByAid(StpUtil.getLoginIdAsLong());	
		// 开始添加
		adminMapper.add(admin);
		// 获取主键
		long id = Jiuan.publicMapper.getPrimarykey();
		// 更改密码（md5与明文）
		adminPasswordService.updatePassword(id, admin.getPassword2());
		
		// 返回主键 
		return id;
	}
	
	
	
	
}
