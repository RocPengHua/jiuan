package com.peng.jiuan.project.redis4console;

import java.util.List;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.peng.jiuan.result.AjaxJson;
import com.peng.jiuan.result.ResultMap;
import com.peng.jiuan.satoken.AuthConstant;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * redis相关操作 
 * @author kong 
 *
 */
@RestController
@RequestMapping("/RedisConsole/")
public class RedisConsoleController {

	/** 获取一些基本预览信息  */
	@PostMapping("getPreInfo")
	@SaCheckPermission(AuthConstant.REDIS_CONSOLE)
	public AjaxJson getPreInfo() {
		ResultMap so = RedisConsoleUtil.getInfo();
		return AjaxJson.getSuccessData(so);
	}

	/** 查询key集合   */
	@PostMapping("getKeys")
	@SaCheckPermission(AuthConstant.REDIS_CONSOLE)
	public AjaxJson getKeys(String k) {
		List<String> keys = RedisConsoleUtil.getKeys(k);
		return AjaxJson.getSuccessData(keys);
	}
	
	/** 查询某个值的详细信息  */
	@PostMapping("getByKey")
	@SaCheckPermission(AuthConstant.REDIS_CONSOLE)
	public AjaxJson getByKey(String key) {
		ResultMap soMap = RedisConsoleUtil.getByKey(key);
		return AjaxJson.getSuccessData(soMap);
	}
	
	/** 添加一个键值  */
	@PostMapping("set")
	@SaCheckPermission(AuthConstant.REDIS_CONSOLE)
	public AjaxJson set(String key, String value, long ttl) {
		RedisConsoleUtil.setBySeconds(key, value, ttl);
		return AjaxJson.getSuccess();
	}

	/** 删除一个键值  */
	@PostMapping("del")
	@SaCheckPermission(AuthConstant.REDIS_CONSOLE)
	public AjaxJson del(String key) {
		RedisConsoleUtil.del(key);
		return AjaxJson.getSuccess();
	}
	
	/** 修改一个值的value  */
	@PostMapping("updateValue")
	@SaCheckPermission(AuthConstant.REDIS_CONSOLE)
	public AjaxJson updateValue(String key, String value) {
		RedisConsoleUtil.updateValue(key, value);
		return AjaxJson.getSuccess();
	}
	
	/** 修改一个值的ttl  */
	@PostMapping("updateTtl")
	@SaCheckPermission(AuthConstant.REDIS_CONSOLE)
	public AjaxJson updateTtl(String key, long ttl) {
		RedisConsoleUtil.updateTtl(key, ttl);
		return AjaxJson.getSuccess();
	}
	
	/** 删除多个键值  */
	@PostMapping("deleteByKeys")
	@SaCheckPermission(AuthConstant.REDIS_CONSOLE)
	public AjaxJson deleteByKeys(@RequestParam(value="key[]") List<String> key) {
		for (String k : key) {
			RedisConsoleUtil.del(k);
		}
		return AjaxJson.getSuccess();
	}
	
}
