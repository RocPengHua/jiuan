package com.peng.jiuan.project.ser_goods;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.peng.jiuan.result.AjaxException;
import java.util.*;

/**
 * 工具类：ser_goods -- 商品表
 * @author peng 
 *
 */
@Component
public class SerGoodsUtil {

	
	/** 底层 Mapper 对象 */
	public static SerGoodsMapper serGoodsMapper;
	@Autowired
	private void setSerGoodsMapper(SerGoodsMapper serGoodsMapper) {
		SerGoodsUtil.serGoodsMapper = serGoodsMapper;
	}
	
	
	/** 
	 * 将一个 SerGoods 对象进行进行数据完整性校验 (方便add/update等接口数据校验) [G] 
	 */
	static void check(SerGoods s) {
		AjaxException.throwByIsNull(s.id, "[记录id] 不能为空");		// 验证: 记录id 
		AjaxException.throwByIsNull(s.name, "[商品名称] 不能为空");		// 验证: 商品名称 
		AjaxException.throwByIsNull(s.avatar, "[商品头像] 不能为空");		// 验证: 商品头像 
		AjaxException.throwByIsNull(s.imageList, "[轮播图片] 不能为空");		// 验证: 轮播图片 
		AjaxException.throwByIsNull(s.content, "[图文介绍] 不能为空");		// 验证: 图文介绍 
		AjaxException.throwByIsNull(s.money, "[商品价格] 不能为空");		// 验证: 商品价格 
		AjaxException.throwByIsNull(s.typeId, "[所属分类] 不能为空");		// 验证: 所属分类 
		AjaxException.throwByIsNull(s.stockCount, "[剩余库存] 不能为空");		// 验证: 剩余库存 
		AjaxException.throwByIsNull(s.status, "[商品状态] 不能为空");		// 验证: 商品状态 (1=上架,2=下架) 
		AjaxException.throwByIsNull(s.createTime, "[创建日期] 不能为空");		// 验证: 创建日期 
		AjaxException.throwByIsNull(s.updateTime, "[更新日期] 不能为空");		// 验证: 更新日期 
	}

	/** 
	 * 获取一个SerGoods (方便复制代码用) [G] 
	 */ 
	static SerGoods getSerGoods() {
		SerGoods s = new SerGoods();	// 声明对象 
		s.id = 0L;		// 记录id 
		s.name = "";		// 商品名称 
		s.avatar = "";		// 商品头像 
		s.imageList = "";		// 轮播图片 
		s.content = "";		// 图文介绍 
		s.money = 0;		// 商品价格 
		s.typeId = 0L;		// 所属分类 
		s.stockCount = 0;		// 剩余库存 
		s.status = 0;		// 商品状态 (1=上架,2=下架) 
		s.createTime = new Date();		// 创建日期 
		s.updateTime = new Date();		// 更新日期 
		return s;
	}
	
	
	
	
	
}
