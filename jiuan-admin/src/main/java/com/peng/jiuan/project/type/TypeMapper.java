package com.peng.jiuan.project.type;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.peng.jiuan.result.ResultMap;
import org.springframework.stereotype.Repository;

/**
 * Mapper: sys_type -- 分类表
 * @author peng 
 */

@Mapper
@Repository
public interface TypeMapper {

	/**
	 * 增  
	 * @param t 实体对象 
	 * @return 受影响行数 
	 */
	int add(Type t);

	/**
	 * 删  
	 * @param id 要删除的数据id  
	 * @return 受影响行数 
	 */
	int delete(Long id);	 

	/** 
	 * 改  
	 * @param t 实体对象 
	 * @return 受影响行数 
	 */
	int update(Type t);

	/** 
	 * 查 - 根据id  
	 * @param id 要查询的数据id 
	 * @return 实体对象 
	 */
	Type getById(Long id);	 

	/**
	 * 查集合 - 根据条件（参数为空时代表忽略指定条件）
	 * @param so 参数集合 
	 * @return 数据列表 
	 */
	List<Type> getList(ResultMap so);


}
