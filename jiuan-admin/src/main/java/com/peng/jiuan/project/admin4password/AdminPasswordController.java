package com.peng.jiuan.project.admin4password;

import com.peng.jiuan.config.SystemObject;
import com.peng.jiuan.project.admin.Admin;
import com.peng.jiuan.project.admin.AdminUtil;
import com.peng.jiuan.result.AjaxJson;
import com.peng.jiuan.utils.JiuanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * admin表 密码相关 
 * 
 */
@RestController
@RequestMapping("/AdminPassword/")
public class AdminPasswordController {

	
	@Autowired
	AdminPasswordService adminPasswordService;

	/** 指定用户修改自己密码 */
	@PostMapping("update")
	AjaxJson updatePassword(String oldPwd, String newPwd) {
		// 1、转md5
		Admin a = AdminUtil.getCurrAdmin();
		String oldPwdMd5 = SystemObject.getPasswordMd5(a.getId(), oldPwd);
		
		// 2、验证
		if(JiuanUtil.isNull(a.getPassword2()) && JiuanUtil.isNull(oldPwd)) {
			// 如果没有旧密码，则不用取验证 
		} else {
			if(!oldPwdMd5.equals(a.getPassword2())) {
				return AjaxJson.getError("旧密码输入错误");
			}
		}
		
		// 3、开始改 
		int line = adminPasswordService.updatePassword(a.getId(), newPwd);
		return AjaxJson.getByLine(line);
	}
	
	
	
	
}
