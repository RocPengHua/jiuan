package com.peng.jiuan.project.role4permission;

import java.util.List;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.annotation.SaMode;
import com.peng.jiuan.project.role.RoleUtil;
import com.peng.jiuan.result.AjaxJson;
import com.peng.jiuan.satoken.AuthConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller: 角色与权限的中间表 
 * @author kong
 *
 */
@RestController
@RequestMapping("/RolePermission/")
public class RolePermissionController {

	
	/** 底层Service */
	@Autowired
	RolePermissionService rolePermissionService;
	
	
	
	/**
	 * 拉取权限id列表  根据指定roleId 
	 * @param roleId
	 * @return
	 */
	@PostMapping("getPcodeByRid")
	@SaCheckPermission(value = {AuthConstant.R1,AuthConstant.ROLE_LIST},mode = SaMode.AND)
    public AjaxJson getPcodeByRid(@RequestParam(defaultValue="0") long roleId){
		// 防止拉出全部
		if(roleId == 0){
			return AjaxJson.getError("roleId不能为null或0");		
		}
		return AjaxJson.getSuccessData(rolePermissionService.getPcodeByRid2(roleId));
	}
	
	
	/** 拉取菜单id列表  根据当前用户roleId  */
	@PostMapping("getPcodeByCurrRid")
	public AjaxJson getPcodeByCurrRid(){
		long roleId = RoleUtil.getCurrRoleId();
		List<Object> list = rolePermissionService.getPcodeByRid2(roleId);
		return AjaxJson.getSuccessData(list);
	}
	

	/**
	 * 修改指定角色的拥有的权限 
	 * @param roleId 角色id
	 * @param code 拥有的权限码集合 
	 * @return
	 */
	@PostMapping("updatePcodeByRid")
	@SaCheckPermission(value = {AuthConstant.R1,AuthConstant.ROLE_LIST},mode = SaMode.AND)
	public AjaxJson updatePcodeByRid(long roleId, String[] code){
		return AjaxJson.getSuccessData(rolePermissionService.updateRoleMenu(roleId, code));
	}
	


	
	


	
	
	
}
