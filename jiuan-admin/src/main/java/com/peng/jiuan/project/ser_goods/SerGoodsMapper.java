package com.peng.jiuan.project.ser_goods;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.peng.jiuan.result.ResultMap;
import org.springframework.stereotype.Repository;

/**
 * Mapper: ser_goods -- 商品表
 * @author peng 
 */

@Mapper
@Repository
public interface SerGoodsMapper {

	/**
	 * 增  
	 * @param s 实体对象 
	 * @return 受影响行数 
	 */
	int add(SerGoods s);

	/**
	 * 删  
	 * @param id 要删除的数据id  
	 * @return 受影响行数 
	 */
	int delete(Long id);	 

	/** 
	 * 改  
	 * @param s 实体对象 
	 * @return 受影响行数 
	 */
	int update(SerGoods s);

	/** 
	 * 查 - 根据id  
	 * @param id 要查询的数据id 
	 * @return 实体对象 
	 */
	SerGoods getById(Long id);	 

	/**
	 * 查集合 - 根据条件（参数为空时代表忽略指定条件）
	 * @param so 参数集合 
	 * @return 数据列表 
	 */
	List<SerGoods> getList(ResultMap so);


}
