package com.peng.jiuan.project.dept;

import java.util.List;

import com.peng.jiuan.project.public4mapper.Jiuan;
import com.peng.jiuan.result.AjaxException;
import com.peng.jiuan.result.AjaxJson;
import com.peng.jiuan.result.ResultMap;
import com.peng.jiuan.satoken.StpUserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import cn.dev33.satoken.annotation.SaCheckPermission;


/**
 * Controller: sys_dept -- 部门表
 * @author peng
 */
@RestController
@RequestMapping("/Dept/")
public class DeptController {

	/** 底层 Mapper 对象 */
	@Autowired
	DeptMapper deptMapper;

	/** 增 */
	@PostMapping("add")
	@SaCheckPermission(Dept.PERMISSION_CODE)
	@Transactional(rollbackFor = Exception.class)
	public AjaxJson add(Dept d){
		deptMapper.add(d);
		d = deptMapper.getById(Jiuan.publicMapper.getPrimarykey());
		return AjaxJson.getSuccessData(d);
	}

	/** 删 */
	@PostMapping("delete")
	@SaCheckPermission(Dept.PERMISSION_CODE)
	public AjaxJson delete(Long id){
		int line = deptMapper.delete(id);
		return AjaxJson.getByLine(line);
	}

	/** 删 - 根据id列表 */
	@PostMapping("deleteByIds")
	@SaCheckPermission(Dept.PERMISSION_CODE)
	public AjaxJson deleteByIds(){
		List<Long> ids = ResultMap.getRequestResultMap().getListByComma("ids", long.class);
		int line = Jiuan.publicMapper.deleteByIds(Dept.TABLE_NAME, ids);
		return AjaxJson.getByLine(line);
	}

	/** 改 */
	@PostMapping("update")
	@SaCheckPermission(Dept.PERMISSION_CODE)
	public AjaxJson update(Dept d){
		int line = deptMapper.update(d);
		return AjaxJson.getByLine(line);
	}

	/** 查 - 根据id */
	@PostMapping("getById")
	@SaCheckPermission(Dept.PERMISSION_CODE)
	public AjaxJson getById(Long id){
		Dept d = deptMapper.getById(id);
		return AjaxJson.getSuccessData(d);
	}

	/** 查集合 - 根据条件（参数为空时代表忽略指定条件） */
	@PostMapping("getList")
	@SaCheckPermission(Dept.PERMISSION_CODE)
	public AjaxJson getList() {
		ResultMap so = ResultMap.getRequestResultMap();
		List<Dept> list = deptMapper.getList(so.startPage());
		return AjaxJson.getPageData(so.getDataCount(), list);
	}

	/** 查集合 (整个表数据转化为tree结构返回) */
	@PostMapping("getTree")
	@SaCheckPermission(Dept.PERMISSION_CODE)
	public AjaxJson getTree() {
		// 获取记录
		ResultMap so = ResultMap.getRequestResultMap();
		List<Dept> list = deptMapper.getList(so);
		// 转为tree结构，并返回
		List<ResultMap> listMap = ResultMap.getResultMapByList(list);
		List<ResultMap> listTree = ResultMap.listToTree(listMap, "id", "parentId", "children");
		return AjaxJson.getPageData(Long.valueOf(listMap.size()), listTree);
	}


	// ------------------------- 前端接口 -------------------------


	/** 改 - 不传不改 [G] */
	@PostMapping("updateByNotNull")
	@SaCheckPermission(Dept.PERMISSION_CODE)
	public AjaxJson updateByNotNull(Long id){

		AjaxException.throwBy(true, "如需正常调用此接口，请删除此行代码");
		// 鉴别身份，是否为数据创建者
		long userId = Jiuan.publicMapper.getColumnByIdToLong(Dept.TABLE_NAME, "user_id", id);
		AjaxException.throwBy(userId != StpUserUtil.getLoginIdAsLong(), "此数据您无权限修改");
		// 开始修改 (请只保留需要修改的字段)
		ResultMap so = ResultMap.getRequestResultMap();
		so.clearNotIn("id", "name", "intro", "parentId", "createTime").clearNull().humpToLineCase();
		int line = Jiuan.publicMapper.updateByResultMapById(Dept.TABLE_NAME, so, id);
		return AjaxJson.getByLine(line);
	}







}