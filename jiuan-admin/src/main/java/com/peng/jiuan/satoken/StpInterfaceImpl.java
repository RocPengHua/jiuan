package com.peng.jiuan.satoken;

import cn.dev33.satoken.stp.StpInterface;
import cn.dev33.satoken.stp.StpUtil;
import com.peng.jiuan.project.admin.AdminMapper;
import com.peng.jiuan.project.role4permission.RolePermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 自定义Sa-Token权限认证接口扩展 
 * 
 * @author kong
 *
 */
@Component	
public class StpInterfaceImpl implements StpInterface {

	@Autowired
	AdminMapper adminMapper;
	
	@Autowired
	RolePermissionService rolePermissionService;
	
	/** 返回一个账号所拥有的权限码集合  */
	@Override
	public List<String> getPermissionList(Object loginId, String loginType) {
		if(loginType.equals(StpUtil.TYPE)) {
			long roleId = adminMapper.getById(Long.valueOf(loginId.toString())).getRoleId();
			return rolePermissionService.getPcodeByRid(roleId);
		}
		return null;
	}
	
	/** 返回一个账号所拥有的角色标识集合  */
	@Override
	public List<String> getRoleList(Object loginId, String loginType) {
		return null;
	}

}
