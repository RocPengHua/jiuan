package com.peng.jiuan.satoken;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * @author RocPengHua
 * @date 2022/7/3 11:47
 */
@Component
public class TestSentinel {

    @Value("${spring.cloud.sentinel.datasource.flow.nacos.namespace:}")
    private String namespace;

    public void print(){
        System.out.println(namespace);
    }
}
