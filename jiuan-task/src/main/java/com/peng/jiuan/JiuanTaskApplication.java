package com.peng.jiuan;

import com.peng.jiuan.utils.JiuanCloudUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableCaching // 启用缓存
@EnableScheduling // 启动定时任务
@SpringBootApplication // springboot本尊
@EnableTransactionManagement // 启动注解事务管理
@EnableFeignClients        // 启用Feign实现RPC调用
public class JiuanTaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(JiuanTaskApplication.class, args);
        JiuanCloudUtil.printCurrentServiceInfo();
    }

}
