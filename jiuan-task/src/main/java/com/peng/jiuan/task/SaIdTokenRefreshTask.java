package com.peng.jiuan.task;

import cn.dev33.satoken.id.SaIdUtil;
import com.peng.jiuan.utils.JiuanUtil;
import com.peng.jiuan.utils.LogUtil;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * Describe: Id-Token定时刷新
 * Created by AlexP on 2021/11/12.
 */
@Configuration
public class SaIdTokenRefreshTask {
    //每隔五分钟刷新Id-Token
    @Scheduled(cron = "0 0/5 * * * ? ")
    public void refreshToken(){
        SaIdUtil.refreshToken();
        LogUtil.info("============= 执行完毕：刷新网关鉴权token：" + JiuanUtil.getNow());
    }
}
