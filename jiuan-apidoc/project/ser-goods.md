# 商品表


---
### 1、增加
- 接口
``` api
	/SerGoods/add
```
- 参数
``` p
	{Long}	id			记录id 
	{String}	name			商品名称 
	{String}	avatar			商品头像 
	{String}	imageList			轮播图片 
	{String}	content			图文介绍 
	{Integer}	money			商品价格 
	{Long}	typeId			所属分类 
	{Integer}	stockCount			剩余库存 
	{Integer}	status			商品状态(1=上架,2=下架) 
	{Date}	createTime			创建日期 
	{Date}	updateTime			更新日期 
```
- 返回 
@import(res)


--- 
### 2、删除
- 接口
``` api
	/SerGoods/delete
```
- 参数
``` p
	{Long}	id			要删除的记录id
```
- 返回
@import(res)


---
### 3、批量删除
- 接口
``` api
	/SerGoods/deleteByIds
```
- 参数
``` p
	{数组}	ids			要删除的记录id数组，逗号隔开，例：ids=1,2,3,4
```
- 返回
@import(res)


---
### 4、修改
- 接口
``` api
	/SerGoods/update
```
- 参数
``` p
	{Long}	id			记录id  (修改条件)
	{String}	name			商品名称 
	{String}	avatar			商品头像 
	{String}	imageList			轮播图片 
	{String}	content			图文介绍 
	{Integer}	money			商品价格 
	{Long}	typeId			所属分类 
	{Integer}	stockCount			剩余库存 
	{Integer}	status			商品状态(1=上架,2=下架) 
	{Date}	createTime			创建日期 
	{Date}	updateTime			更新日期 
```
- 返回
@import(res)


---
### 5、查 - 根据id
- 接口
```  api 
	/SerGoods/getById
```
- 参数
``` p
	{Long}	id			要查询的记录id
```
- 返回示例
``` js
	{
		"code": 200,
		"msg": "ok",
		"data": {
			"id": 0L,		// 记录id
			"name": "",		// 商品名称
			"avatar": "",		// 商品头像
			"imageList": "",		// 轮播图片
			"content": "",		// 图文介绍
			"money": 0,		// 商品价格
			"typeId": 0L,		// 所属分类
			"stockCount": 0,		// 剩余库存
			"status": 0,		// 商品状态(1=上架,2=下架)
			"createTime": new Date(),		// 创建日期
			"updateTime": new Date(),		// 更新日期
			"typeName": "",			// 所属分类
		},
		"dataCount": -1
	}
```


---
### 6、查集合 - 根据条件
- 接口
``` api
	/SerGoods/getList
```
- 参数 （参数为空时代表忽略指定条件）
``` p
	{int}	pageNo = 1			当前页
	{int}	pageSize = 10		页大小 
	{Long}	id			记录id 
	{String}	name			商品名称 
	{String}	avatar			商品头像 
	{String}	imageList			轮播图片 
	{String}	content			图文介绍 
	{Integer}	money			商品价格 
	{Long}	typeId			所属分类 
	{Integer}	stockCount			剩余库存 
	{Integer}	status			商品状态(1=上架,2=下架) 
	{Date}	createTime			创建日期 
	{Date}	updateTime			更新日期 
	{int}	sortType = 0		排序方式 (0 = 默认, 1 = 记录id, 2 = 商品名称, 3 = 商品价格, 4 = 所属分类, 5 = 剩余库存, 6 = 商品状态(1=上架,2=下架), 7 = 创建日期, 8 = 更新日期)
```
- 返回 
``` js
	{
		"code": 200,
		"msg": "ok",
		"data": [
			// 数据列表，格式参考getById 
		],
		"dataCount": 100	// 数据总数
	}
```




---
### 7、修改 - 空值不改
- 接口
``` api
	/SerGoods/updateByNotNull
```
- 参数
``` p
	{Long}	id			记录id  (修改条件)
	{String}	name			商品名称 
	{String}	avatar			商品头像 
	{String}	imageList			轮播图片 
	{String}	content			图文介绍 
	{Integer}	money			商品价格 
	{Long}	typeId			所属分类 
	{Integer}	stockCount			剩余库存 
	{Integer}	status			商品状态(1=上架,2=下架) 
	{Date}	createTime			创建日期 
	{Date}	updateTime			更新日期 
```
- 返回
@import(res)







