# 系统管理员表


---
### 1、登录
- 接口
``` api
	/AccAdmin/doLogin
```
- 参数
``` p
	{String}	key			账号 
	{String}	password	密码
```
- 返回 
@import(res)


--- 
### 2、登出
- 接口
``` api
	/AccAdmin/doExit
```
- 参数
``` p
	
```
- 返回
@import(res)






