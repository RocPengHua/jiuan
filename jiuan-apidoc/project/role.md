# 系统角色表


---
### 1、增加
- 接口
``` api
	/Role/add
```
- 参数
``` p
	{Long}	id			角色id，--主键、自增 
	{String}	name			角色名称,唯一约束 
	{String}	info			角色详细描述 
	{Integer}	isLock			是否锁定(1=是,2=否),锁定之后不可随意删除,防止用户误操作 
	{String}	createTime			创建时间 
```
- 返回 
@import(res)


--- 
### 2、删除
- 接口
``` api
	/Role/delete
```
- 参数
``` p
	{Long}	id			要删除的记录id
```
- 返回
@import(res)


---
### 3、批量删除
- 接口
``` api
	/Role/deleteByIds
```
- 参数
``` p
	{数组}	ids			要删除的记录id数组，逗号隔开，例：ids=1,2,3,4
```
- 返回
@import(res)


---
### 4、修改
- 接口
``` api
	/Role/update
```
- 参数
``` p
	{Long}	id			角色id，--主键、自增  (修改条件)
	{String}	name			角色名称,唯一约束 
	{String}	info			角色详细描述 
	{Integer}	isLock			是否锁定(1=是,2=否),锁定之后不可随意删除,防止用户误操作 
	{String}	createTime			创建时间 
```
- 返回
@import(res)


---
### 5、查 - 根据id
- 接口
```  api 
	/Role/getById
```
- 参数
``` p
	{Long}	id			要查询的记录id
```
- 返回示例
``` js
	{
		"code": 200,
		"msg": "ok",
		"data": {
			"id": 0L,		// 角色id，--主键、自增
			"name": "",		// 角色名称,唯一约束
			"info": "",		// 角色详细描述
			"isLock": 0,		// 是否锁定(1=是,2=否),锁定之后不可随意删除,防止用户误操作
			"createTime": "",		// 创建时间
		},
		"dataCount": -1
	}
```


---
### 6、查集合 - 根据条件
- 接口
``` api
	/Role/getList
```
- 参数 （参数为空时代表忽略指定条件）
``` p
	{int}	pageNo = 1			当前页
	{int}	pageSize = 10		页大小 
	{Long}	id			角色id，--主键、自增 
	{String}	name			角色名称,唯一约束 
	{String}	info			角色详细描述 
	{Integer}	isLock			是否锁定(1=是,2=否),锁定之后不可随意删除,防止用户误操作 
	{String}	createTime			创建时间 
	{int}	sortType = 0		排序方式 (0 = 默认, 1 = 角色id，--主键、自增, 2 = 角色名称,唯一约束, 3 = 角色详细描述, 4 = 是否锁定(1=是,2=否),锁定之后不可随意删除,防止用户误操作, 5 = 创建时间)
```
- 返回 
``` js
	{
		"code": 200,
		"msg": "ok",
		"data": [
			// 数据列表，格式参考getById 
		],
		"dataCount": 100	// 数据总数
	}
```




---
### 7、修改 - 空值不改
- 接口
``` api
	/Role/updateByNotNull
```
- 参数
``` p
	{Long}	id			角色id，--主键、自增  (修改条件)
	{String}	name			角色名称,唯一约束 
	{String}	info			角色详细描述 
	{Integer}	isLock			是否锁定(1=是,2=否),锁定之后不可随意删除,防止用户误操作 
	{String}	createTime			创建时间 
```
- 返回
@import(res)







