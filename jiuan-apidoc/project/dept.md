# 部门表


---
### 1、增加
- 接口
``` api
	/Dept/add
```
- 参数
``` p
	{Long}	id			记录id 
	{String}	name			部门名称 
	{String}	intro			部门介绍 
	{Long}	parentId			父部门id 
	{Date}	createTime			创建日期 
```
- 返回 
@import(res)


--- 
### 2、删除
- 接口
``` api
	/Dept/delete
```
- 参数
``` p
	{Long}	id			要删除的记录id
```
- 返回
@import(res)


---
### 3、批量删除
- 接口
``` api
	/Dept/deleteByIds
```
- 参数
``` p
	{数组}	ids			要删除的记录id数组，逗号隔开，例：ids=1,2,3,4
```
- 返回
@import(res)


---
### 4、修改
- 接口
``` api
	/Dept/update
```
- 参数
``` p
	{Long}	id			记录id  (修改条件)
	{String}	name			部门名称 
	{String}	intro			部门介绍 
	{Long}	parentId			父部门id 
	{Date}	createTime			创建日期 
```
- 返回
@import(res)


---
### 5、查 - 根据id
- 接口
```  api 
	/Dept/getById
```
- 参数
``` p
	{Long}	id			要查询的记录id
```
- 返回示例
``` js
	{
		"code": 200,
		"msg": "ok",
		"data": {
			"id": 0L,		// 记录id
			"name": "",		// 部门名称
			"intro": "",		// 部门介绍
			"parentId": 0L,		// 父部门id
			"createTime": new Date(),		// 创建日期
		},
		"dataCount": -1
	}
```


---
### 6、查集合 - 根据条件
- 接口
``` api
	/Dept/getList
```
- 参数 （参数为空时代表忽略指定条件）
``` p
	{int}	pageNo = 1			当前页
	{int}	pageSize = 10		页大小 
	{Long}	id			记录id 
	{String}	name			部门名称 
	{String}	intro			部门介绍 
	{Long}	parentId			父部门id 
	{Date}	createTime			创建日期 
	{int}	sortType = 0		排序方式 (0 = 默认, 1 = 记录id, 2 = 部门名称, 3 = 父部门id, 4 = 创建日期)
```
- 返回 
``` js
	{
		"code": 200,
		"msg": "ok",
		"data": [
			// 数据列表，格式参考getById 
		],
		"dataCount": 100	// 数据总数
	}
```




---
### 7、修改 - 空值不改
- 接口
``` api
	/Dept/updateByNotNull
```
- 参数
``` p
	{Long}	id			记录id  (修改条件)
	{String}	name			部门名称 
	{String}	intro			部门介绍 
	{Long}	parentId			父部门id 
	{Date}	createTime			创建日期 
```
- 返回
@import(res)







