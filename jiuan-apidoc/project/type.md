# 分类表


---
### 1、增加
- 接口
``` api
	/Type/add
```
- 参数
``` p
	{Long}	id			记录id 
	{String}	name			分类名字 
	{String}	icon			分类图标 
	{Integer}	sort			排序索引 
	{Date}	createTime			创建日期 
```
- 返回 
@import(res)


--- 
### 2、删除
- 接口
``` api
	/Type/delete
```
- 参数
``` p
	{Long}	id			要删除的记录id
```
- 返回
@import(res)


---
### 3、批量删除
- 接口
``` api
	/Type/deleteByIds
```
- 参数
``` p
	{数组}	ids			要删除的记录id数组，逗号隔开，例：ids=1,2,3,4
```
- 返回
@import(res)


---
### 4、修改
- 接口
``` api
	/Type/update
```
- 参数
``` p
	{Long}	id			记录id  (修改条件)
	{String}	name			分类名字 
	{String}	icon			分类图标 
	{Integer}	sort			排序索引 
	{Date}	createTime			创建日期 
```
- 返回
@import(res)


---
### 5、查 - 根据id
- 接口
```  api 
	/Type/getById
```
- 参数
``` p
	{Long}	id			要查询的记录id
```
- 返回示例
``` js
	{
		"code": 200,
		"msg": "ok",
		"data": {
			"id": 0L,		// 记录id
			"name": "",		// 分类名字
			"icon": "",		// 分类图标
			"sort": 0,		// 排序索引
			"createTime": new Date(),		// 创建日期
			"serGoodsCount": "",			// 商品数量
		},
		"dataCount": -1
	}
```


---
### 6、查集合 - 根据条件
- 接口
``` api
	/Type/getList
```
- 参数 （参数为空时代表忽略指定条件）
``` p
	{int}	pageNo = 1			当前页
	{int}	pageSize = 10		页大小 
	{Long}	id			记录id 
	{String}	name			分类名字 
	{String}	icon			分类图标 
	{Integer}	sort			排序索引 
	{Date}	createTime			创建日期 
	{int}	sortType = 0		排序方式 (0 = 默认, 1 = 记录id, 2 = 分类名字, 3 = 排序索引, 4 = 创建日期, 5 = 商品数量)
```
- 返回 
``` js
	{
		"code": 200,
		"msg": "ok",
		"data": [
			// 数据列表，格式参考getById 
		],
		"dataCount": 100	// 数据总数
	}
```




---
### 7、修改 - 空值不改
- 接口
``` api
	/Type/updateByNotNull
```
- 参数
``` p
	{Long}	id			记录id  (修改条件)
	{String}	name			分类名字 
	{String}	icon			分类图标 
	{Integer}	sort			排序索引 
	{Date}	createTime			创建日期 
```
- 返回
@import(res)







