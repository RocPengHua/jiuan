package com.peng.jiuan;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 监控台
 * @author 彭鹏
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableAdminServer
public class JiuanBootAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(JiuanBootAdminApplication.class, args);
        System.out.println("\n--------------- JiuanBootAdminApplication 监控台 启动 ---------------\n");
    }

}
