package com.peng.jiuan.project.apilog;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.peng.jiuan.result.AjaxJson;
import com.peng.jiuan.result.ResultMap;
import com.peng.jiuan.satoken.AuthConstant;
import com.peng.jiuan.project.public4mapper.Jiuan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Controller: api请求记录表
 * @author kong 
 */
@RestController
@RequestMapping("/JiuanApilog/")
public class JiuanApilogControlle {

	/** 底层 Mapper 对象 */
	@Autowired
	JiuanApilogMapper jiuanApilogMapper;
	

	/** 删 */
	@PostMapping("delete")
	@SaCheckPermission(AuthConstant.APILOG_LIST)
	AjaxJson delete(String id){
		int line = jiuanApilogMapper.delete(id);
		return AjaxJson.getByLine(line);
	}

	/** 删 - 根据id列表 */
	@PostMapping("deleteByIds")
	@SaCheckPermission(AuthConstant.APILOG_LIST)
	AjaxJson deleteByIds(){
		// 鉴权
		// 开始删除
		List<Long> ids = ResultMap.getRequestResultMap().getListByComma("ids", long.class);
		int line = Jiuan.publicMapper.deleteByIds("jiuan_apilog", ids);
		return AjaxJson.getByLine(line);
	}
	
	/** 删 - 根据日期范围 */
	@PostMapping("deleteByStartEnd")
	@SaCheckPermission(AuthConstant.APILOG_LIST)
	AjaxJson deleteByStartEnd(String startTime, String endTime){
		int line = jiuanApilogMapper.deleteByStartEnd(startTime, endTime);
		return AjaxJson.getSuccessData(line);
	}
	
	/** 查 - 集合（参数为null或0时默认忽略此条件）   */
	@PostMapping("getList")
	@SaCheckPermission(AuthConstant.APILOG_LIST)
	AjaxJson getList() { 
		ResultMap so = ResultMap.getRequestResultMap();
		List<JiuanApilog> list = jiuanApilogMapper.getList(so.startPage());
		return AjaxJson.getPageData(so.getDataCount(), list);
	}

	/** 统计  */
	@PostMapping("staBy")
	@SaCheckPermission(AuthConstant.APILOG_LIST)
	AjaxJson staBy() { 
		ResultMap so = ResultMap.getRequestResultMap();
		ResultMap data = jiuanApilogMapper.staBy(so);
		return AjaxJson.getSuccessData(data);
	}
	

	
	
}
