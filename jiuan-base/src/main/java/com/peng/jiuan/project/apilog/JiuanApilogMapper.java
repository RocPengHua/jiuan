package com.peng.jiuan.project.apilog;

import com.peng.jiuan.result.ResultMap;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Mapper: api请求记录表
 */
@Mapper
public interface JiuanApilogMapper {

	/**
	 * 保存入库 
	 * @param apiLog
	 * @return
	 */
	int saveObj(JiuanApilog apiLog);
	
	/**
	 * 增 
	 * @param apiLog
	 * @return
	 */
	int add(JiuanApilog apiLog);

	/**
	 * 删 
	 * @param id
	 * @return
	 */
	int delete(String id);	 

	/**
	 * 删 - 根据日期范围 
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	int deleteByStartEnd(@Param("startTime")String startTime, @Param("endTime")String endTime);
	
	/**
	 * 改 
	 * @param apiLog
	 * @return
	 */
	int update(JiuanApilog apiLog);
	
	/**
	 * 查 - 集合 
	 * @param so
	 * @return
	 */
	List<JiuanApilog> getList(ResultMap so);

	/**
	 * 查 - 集合 
	 * @param so
	 * @return
	 */
	ResultMap staBy(ResultMap so);


}
