package com.peng.jiuan.project.public4mapper;

/**
 * 以lambda表达式开启事务的辅助类
 *
 */
public interface JdbcLambdaBegin {

	/**
	 * 执行事务的方法 
	 */
	public void run();
	
	
}
