package com.peng.jiuan.project.test;

import cn.dev33.satoken.stp.StpUtil;
import com.peng.jiuan.result.AjaxJson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 测试controller 
 */
@RestController
@Slf4j
public class TestController {

	
	/**
	 * 测试请求，如果能正常访问此路由，则证明项目已经部署成功 
	 * @return
	 */
	@GetMapping("/test")
	public AjaxJson test() {
		log.info("------------------ 成功进入请求 ------------------");
		log.info(String.valueOf(StpUtil.getTokenInfo()));
		return AjaxJson.getSuccess("请求成功");
	}


	
}
