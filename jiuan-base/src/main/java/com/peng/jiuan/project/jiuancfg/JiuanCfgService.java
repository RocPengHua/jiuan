package com.peng.jiuan.project.jiuancfg;

import com.peng.jiuan.project.public4mapper.Jiuan;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * 配置类：sys_cfg
 *
 */
@Service
public class JiuanCfgService {


	/** 获得cfg_value 根据cfgName */
	@Cacheable(value="jiuan_cfg_", key="#cfgName")
	public String getCfgValue(String cfgName){
		return Jiuan.publicMapper.getColumnByWhere("sys_cfg", "cfg_value", "cfg_name", cfgName);
	}
	
	
	/** 修改cfg_value 根据cfgName */
	@CacheEvict(value="jiuan_cfg_", key="#cfgName")
	public int updateCfgValue(String cfgName, String cfgValue){
		return Jiuan.publicMapper.updateColumnBy("sys_cfg", "cfg_value", cfgValue, "cfg_name", cfgName);
	}
	
	
	
		
	
	
	
	
}
