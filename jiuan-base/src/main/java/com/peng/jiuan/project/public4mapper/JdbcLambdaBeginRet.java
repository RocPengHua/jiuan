package com.peng.jiuan.project.public4mapper;

/**
 * 以lambda表达式开启事务的辅助类
 *
 */
public interface JdbcLambdaBeginRet {
	

	/**
	 * 执行事务的方法 
	 * @return
	 */
	public Object run();
	
	
}
