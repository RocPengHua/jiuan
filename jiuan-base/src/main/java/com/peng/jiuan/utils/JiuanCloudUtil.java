package com.peng.jiuan.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.net.InetAddress;

/**
 * springboot启动之后 
 */
@Order(Integer.MAX_VALUE)
@Component
@Slf4j
public class JiuanCloudUtil {
    
	@Value("${server.port:8080}")
    private String port;

    @Value("${server.servlet.context-path:}")
    private String path;

    @Value("${spring.application.name:}")
    private String applicationName;

    @Value("${spring.profiles.active:}")
    private String active;
   
    
    public void run() throws Exception {
        String ip = InetAddress.getLocalHost().getHostAddress();
        String str = "\n------------- " + applicationName + " (" + active + ") 启动成功 --by " + JiuanUtil.getNow() + " -------------\n" +
                "\t- Local:   http://localhost:" + port + path + "\n" +
                "\t- Local2:  http://127.0.0.1:" + port + path + "\n" +
                "\t- Network: http://" + ip + ":" + port + path + "\n";
        log.info(str);
    }



    
    static JiuanCloudUtil startup;
    @Autowired
	public void setSaPlusStartup(JiuanCloudUtil startup) {
        JiuanCloudUtil.startup = startup;
	}
    
    
    /**
     * 打印当前服务信息 
     */
    public static void printCurrentServiceInfo() {
    	try {
            startup.run();
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
    
    

}

