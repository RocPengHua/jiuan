package com.peng.jiuan.feign;

import com.peng.jiuan.constant.FeignConstant;
import com.peng.jiuan.interceptor.FeignInterceptor;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Describe:
 * Created by AlexP on 2021/11/12.
 */
@FeignClient(
        name = FeignConstant.JIUAN_HOME,                 // 服务名称
        configuration = FeignInterceptor.class,        // 请求拦截器 （关键代码）
        fallback = SpCfgInterfaceFallback.class         // 服务降级处理

)
public interface SpCfgInterface {

    // 获取server端指定配置信息
    @RequestMapping("/SpCfgApi/getServerCfg")
    public String getServerCfg(@RequestParam("key")String key, @RequestParam("defaultValue")String defaultValue);

    // 获取App端指定配置信息
    @RequestMapping("/SpCfgApi/getAppCfg")
    public String getAppCfg(@RequestParam("key")String key, @RequestParam("defaultValue")String defaultValue);

}

