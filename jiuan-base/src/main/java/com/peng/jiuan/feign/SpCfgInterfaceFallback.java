package com.peng.jiuan.feign;

/**
 * Describe:
 * Created by AlexP on 2021/11/12.
 */
public class SpCfgInterfaceFallback implements SpCfgInterface {

    @Override
    public String getServerCfg(String key, String defaultValue) {
        return defaultValue;
    }

    @Override
    public String getAppCfg(String key, String defaultValue) {
        return defaultValue;
    }
}
