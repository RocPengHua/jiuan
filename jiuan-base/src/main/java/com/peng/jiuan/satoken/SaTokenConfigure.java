package com.peng.jiuan.satoken;

import cn.dev33.satoken.jwt.StpLogicJwtForStyle;
import cn.dev33.satoken.stp.StpLogic;
import com.peng.jiuan.result.AjaxJson;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import cn.dev33.satoken.context.SaHolder;
import cn.dev33.satoken.filter.SaServletFilter;
import cn.dev33.satoken.id.SaIdUtil;
import cn.dev33.satoken.interceptor.SaAnnotationInterceptor;

/**
 * Sa-Token 代码方式进行配置 
 */
@Configuration
public class SaTokenConfigure implements WebMvcConfigurer {

	/**
	 * 注册 Sa-Token 的拦截器，打开注解式鉴权功能 
	 */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new SaAnnotationInterceptor()).addPathPatterns("/**"); 
    }

	/**
	 * Sa-Token 整合 jwt (Style模式)
	 * @return
	 */
	@Bean
	public StpLogic getStpLogicJwt() {
		return new StpLogicJwtForStyle();
	}
    /**
     * 注册 Sa-Token全局过滤器，解决跨域问题 
     */
    @Bean
    public SaServletFilter getSaServletFilter() {
        return new SaServletFilter()
        		// 拦截与排除 path 
        		.addInclude("/**").addExclude("/favicon.ico")
        		
        		// 全局认证函数 
        		.setAuth(obj -> {
        			// 校验 Id-Token 身份凭证     —— 以下两句代码可简化为：SaIdUtil.checkCurrentRequestToken(); 
                    String token = SaHolder.getRequest().getHeader(SaIdUtil.ID_TOKEN);
                    SaIdUtil.checkToken(token);
        		})
        		
        		// 异常处理函数  
        		.setError(e -> {
        			return AjaxJson.getError(e.getMessage());
        		})
        		
        		// 前置函数：在每次认证函数之前执行
        		.setBeforeAuth(obj -> {
        			// ---------- 设置跨域响应头 ----------
        			// 这里不需要处理跨域问题，因为我们已经在网关处处理过了  
//        			SaHolder.getResponse()
//        			.setHeader("Access-Control-Allow-Origin", "*")
//        			.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE")
//        			.setHeader("Access-Control-Max-Age", "3600")
//        			.setHeader("Access-Control-Allow-Headers", "*");
//        			
//        			// 如果是预检请求，则立即返回到前端 
//        			SaRouter.match(SaHttpMethod.OPTIONS)
//        				.free(r -> System.out.println("--------OPTIONS预检请求，不做处理"))
//        				.back();
        		})
        		;
    }
    
}
