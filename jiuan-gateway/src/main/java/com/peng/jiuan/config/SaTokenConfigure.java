package com.peng.jiuan.config;

import cn.dev33.satoken.context.SaHolder;
import cn.dev33.satoken.reactor.filter.SaReactorFilter;
import cn.dev33.satoken.router.SaHttpMethod;
import cn.dev33.satoken.router.SaRouter;
import cn.dev33.satoken.util.SaResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 彭鹏
 * @date 2021/11/10.
 * Sa-Token 代码配置
 */
@Configuration
@Slf4j
public class SaTokenConfigure {
    /**
     * 注册全局过滤器
     */
    @Bean
    public SaReactorFilter getSaServletFilter(){
        return new SaReactorFilter()
                //拦截域排除
                .addInclude("/**").addExclude("/favicon.ico")
                //全局认证函数
                .setAuth(obj ->{})
                //全局异常处理
                .setError(e -> SaResult.error(e.getMessage()))
                // 前置函数，在每次认证函数之前执行
                .setBeforeAuth(obj ->{
                    // ---------- 设置跨域响应头 ----------
                    SaHolder.getResponse()
                            // 允许指定域访问跨域资源
                            .setHeader("Access-Control-Allow-Origin", "*")
                            // 允许所有请求方式
                            .setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE")
                            // 有效时间
                            .setHeader("Access-Control-Max-Age", "3600")
                            // 允许的header参数
                            .setHeader("Access-Control-Allow-Headers", "*");

                    // 如果预请求直接返回前端
                    SaRouter.match(SaHttpMethod.OPTIONS)
                            .free(r -> log.info("-------- OPTIONS预检请求，不做处理"))
                            .back();
                });
    }
}
