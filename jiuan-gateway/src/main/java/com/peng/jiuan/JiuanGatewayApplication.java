package com.peng.jiuan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import springfox.documentation.oas.annotations.EnableOpenApi;

/**
 * gateway网关
 * @author 彭鹏
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableOpenApi
public class JiuanGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(JiuanGatewayApplication.class, args);
        System.out.println("\n--------------- JiuanGatewayApplication 服务网关启动成功 ---------------\n");
    }

}
