// 此处定义所有有关 sa-plus 的路由菜单 
// 如需添加自定义菜单，请不要更改此文件，请在 menu-list.js 里添加 (没有这个文件就新建) 
window.menuList = window.menuList || [];
window.menuList.unshift(
	{
		id: 'bas',
		name: '身份相关',
		isShow: false,	// 隐藏显示 
		info: '身份相关权限，不显示在菜单上', 
		childList: [
			{id: '1', name: '身份-超管', isShow: false, info: '最高权限，超管身份的代表（请谨慎授权）'},
			{id: '11', name: '身份-普通账号', isShow: false, info: '无特殊权限'},
			{id: '99', name: '允许进入后台管理', isShow: false, info: '只有拥有这个权限的角色才可以进入后台'},
		]
	},
	{	
		id: 'console',
		name: '监控中心',
		icon: 'el-icon-view',
		info: '对本系统的各种监控',
		childList: [
			{id: 'sql-console', name: 'SQL监控台', url: 'jiuan-view/console/sql-console.html', info: 'sql控制台'},
			{id: 'redis-console', name: 'Redis控制台', url: 'jiuan-view/console/redis-console.html', info: 'redis常用工具'},
			{id: 'apilog-list', name: 'API请求日志', url: 'jiuan-view/apilog/api-log-list.html', info: '记录本系统所有的api请求'},
			{id: 'form-generator', name: '在线表单构建', url: 'https://mrhj.gitee.io/form-generator/#/'},
		]
	},
	{	
		id: 'auth',
		name: '权限控制',
		icon: 'el-icon-unlock',
		info: '对系统角色权限的分配等设计，敏感度较高，请谨慎授权',
		childList: [
			{id: 'role-list', name: '角色列表', url: 'jiuan-view/role/role-list.html', info: '管理系统各种角色'},
			{id: 'menu-list', name: '菜单列表', url: 'jiuan-view/role/menu-list.html', info: '所有菜单项预览'},
			{id: 'admin-list', name: '管理员列表', url: 'jiuan-view/admin/admin-list.html', info: '所有管理员账号'},
		]
	},
	{
		id: 'cfg',
		name: '系统配置', 
		icon: 'el-icon-setting', 
		info: '有关系统的一些配置', 
		childList: [
			{id: 'cfg-app', name: '系统对公配置', url: 'jiuan-view/cfg/app-cfg.html'},
			{id: 'cfg-server', name: '服务器私有配置', url: 'jiuan-view/cfg/server-cfg.html'},
		]
	},
	{
		id: 'dept',
		name: '部门表',
		icon: 'el-icon-crop',
		info: '部门表表数据的维护',
		childList: [
			{id: 'dept-list', name: '部门列表', url: 'jiuan-view/dept/dept-list.html'},
		]
	},
	{
		id: 'type',
		name: '分类表',
		icon: 'el-icon-eleme',
		info: '分类表表数据的维护',
		childList: [
			{id: 'type-list', name: '分类列表', url: 'jiuan-view/type/type-list.html'},
		]
	},
	{
		id: 'ser-goods',
		name: '商品表',
		icon: 'el-icon-apple',
		info: '商品表表数据的维护',
		childList: [
			{id: 'ser-goods-list', name: '商品表-列表', url: 'jiuan-view/ser-goods/ser-goods-list.html'},
		]
	},
);