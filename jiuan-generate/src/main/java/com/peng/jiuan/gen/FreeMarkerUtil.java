package com.peng.jiuan.gen;

import cn.hutool.core.io.file.FileReader;
import com.peng.jiuan.gen.cfg.GenCfgManager;
import freemarker.template.Configuration;
import freemarker.template.Template;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * freemarker操作 的工具类
 * @author kong
 *
 */
public class FreeMarkerUtil {

	
	static String prefix = "freemarker/";		// 路径前缀 
	static String suffix = "";				// 路径后缀 
	
	/**
	 * 读取并返回 
	 * @param fltUrl flt的路径 
	 * @param dataModel 参数集合
	 * @return
	 */
	public static String getResult(String fltUrl, Object dataModel) {
		
		// 1、从文件中读取字符串
		FileReader fileReader = new FileReader(prefix + fltUrl + suffix);
		String str = fileReader.readString();
		
		// 2、让 freemarker解析遍历 
		StringWriter result = new StringWriter();
		try {
			Template t = new Template("template", new StringReader(str), new Configuration(Configuration.VERSION_2_3_23));
			t.process(dataModel, result);
		} catch (Exception e) {
			System.err.println("------------------------------flt文件：" + fltUrl);
			System.err.println("------------------------------dataModel：" + dataModel);
			throw new RuntimeException(e);
		}

		// 3、返回结果 
		return result.toString();
	}
	
	// 重载一下 
	// 因为cfg对象几乎太常用了，几乎每个模板都必须用它，所以封装起来 
	public static String getResult(String fltUrl, String appendKey, Object appendModel ) {
		Map<String, Object> parameMap = new HashMap<String, Object>();
		parameMap.put("cfg", GenCfgManager.cfg);
		parameMap.put(appendKey, appendModel);
		return getResult(fltUrl, parameMap);
	}
	
	
	
}
