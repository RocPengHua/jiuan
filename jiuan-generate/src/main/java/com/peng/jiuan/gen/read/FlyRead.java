package com.peng.jiuan.gen.read;


import com.peng.jiuan.gen.cfg.GenCfg;

/**
 * 读取的接口
 * @author kongyongshun
 *
 */
public interface FlyRead {

	public FlyRead setCodeCfg(GenCfg codeCfg);
	
	/**
	 * 根据CodeCfg配置读取
	 */
	public void readInfo();
	

	
	
	
}
