package com.peng.jiuan.skywalking;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 配置skywalking告警
 * @author RocPengHua
 * @date 2022/7/10 18:02
 */
@RestController
public class AlarmController {

    @RequestMapping("/notify")
    public void notify(@RequestBody Object object){
        //发送通知
        System.out.println("--------------------------------------------------------------------------------------------");
        System.out.println("--------------------------------------------------------------------------------------------");
        System.out.println("--------------------------------------------------------------------------------------------");
        System.out.println(object.toString());
    }
}
