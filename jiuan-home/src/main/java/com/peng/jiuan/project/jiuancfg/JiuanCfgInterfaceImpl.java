package com.peng.jiuan.project.jiuancfg;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 服务实现者
 *
 */
@RestController
@RequestMapping("/JiuanCfgApi/")
public class JiuanCfgInterfaceImpl {

	/**
	 * 获取server端指定配置信息
	 * @param key
	 * @param defaultValue
	 * @return
	 */
	@RequestMapping("getServerCfg")
	public String getServerCfg(String key, String defaultValue) {
		return JiuanCfgUtil.getServerCfg(key, defaultValue);
	}

	/**
	 * 获取App端指定配置信息
	 * @param key
	 * @param defaultValue
	 * @return
	 */
	@RequestMapping("getAppCfg")
	public String getAppCfg(String key, String defaultValue) {
		return JiuanCfgUtil.getAppCfg(key, defaultValue);
	}
	
}
