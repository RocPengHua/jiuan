package com.peng.jiuan.project.jiuancfg;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.peng.jiuan.result.AjaxJson;
import com.peng.jiuan.satoken.AuthConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 系统配置相关 
 *
 */
@RestController
@RequestMapping("/JiuanCfg/")
public class JiuanCfgController {

	private static JiuanCfgService sysCfgService;
	@Autowired
	public void setSysCfgService(JiuanCfgService sysCfgService) {
		JiuanCfgController.sysCfgService = sysCfgService;
	}

	/** 返回指定【cfgName】配置信息 */
	@RequestMapping("getCfg")
	@SaCheckPermission(AuthConstant.CFG)
	public AjaxJson getCfg(String cfgName){
		return AjaxJson.getSuccessData(sysCfgService.getCfgValue(cfgName));
	}
	
	/** 修改指定【cfgName】配置信息  */
	@RequestMapping("updateCfg")
	@SaCheckPermission(AuthConstant.CFG)
	public AjaxJson updateCfg(String cfgName, String cfgValue){
		int a=sysCfgService.updateCfgValue(cfgName, cfgValue);
		return AjaxJson.getByLine(a);
	}


	/** 返回应用配置信息 （对公开放的） */
	@RequestMapping("appCfg")
	public AjaxJson appCfg() throws InterruptedException {
		Thread.sleep(3000);
		return AjaxJson.getSuccessData(sysCfgService.getCfgValue("app_cfg"));
	}
	
	
	
	
	
	
}
